import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { global } from './global';



@Injectable()
export class Service {
    public url: string;
  

    constructor(private _http: HttpClient) {
        this.url = global.url;
    }
    estadisticos(datos): Observable<any> {

        let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        return this._http.post(this.url + 'estadisticos/', datos, { headers: headers });

    }


    // GRAFICOS

    grafico(datos): Observable<any> {

        let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        return this._http.post(this.url + 'grafico/', datos, { headers: headers });

    }
    graficoPM(datos): Observable<any> {

        let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        return this._http.post(this.url + 'graficopm/', datos, { headers: headers });

    }






}